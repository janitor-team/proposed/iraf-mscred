Source: iraf-mscred
Maintainer: Debian Astro Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper (>= 11),
               iraf-dev,
               iraf-noao-dev
Standards-Version: 4.1.5
Vcs-Browser: https://salsa.debian.org/debian-astro-team/iraf-mscred
Vcs-Git: https://salsa.debian.org/debian-astro-team/iraf-mscred.git

Package: iraf-mscred
Architecture: any
Multi-Arch: foreign
Depends: iraf,
         ${misc:Depends},
         ${shlibs:Depends}
Enhances: iraf
Suggests: iraf-fitsutil
Description: CCD mosaic reduction package for IRAF
 The MSCRED external package is used to reduce CCD mosaic data in
 multiextension FITS format. This format is produced for example by
 the Data Capture Agent when observing with the NOAO CCD Mosaic
 Imager, as well as similar instruments from AAO, CFA, CFHT, ESO, INT
 and others.
 .
 The tasks in the mscred package perform the following functions:
 .
  * display the mosaic data as an apparent single mosaic image,
  * provide interactive examination of displayed mosaic data,
  * combine multiple calibration exposures into master calibration files,
  * perform the basic CCD calibrations such as zero level and gain,
  * reconstruct a single mosaic image with distortions removed,
  * register and combine dithered operations, and
  * save and restore data on tape.
